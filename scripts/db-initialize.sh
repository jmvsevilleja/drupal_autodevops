#!/bin/bash

set -e

BASEDIR="$( cd "$(dirname "$0")" ; pwd -P )"

mkdir -p ${BASEDIR}/../web/sites/default/files/translations
chown -R www-data:www-data ${BASEDIR}/../web/sites/default/files
${BASEDIR}/../vendor/bin/drush si standard -y install_configure_form.enable_update_status_emails=NULL --root=${BASEDIR}/../web --site-name=Geek~o~fil --account-name=admin --account-pass=admin --site-mail=no-reply@geek-o-fil.com --locale=fr
