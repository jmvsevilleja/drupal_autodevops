# Drupal Auto DevOps

_This project is a boilerplate to use Drupal with GitLab Auto DevOps features._

## Context

This project:
- Use Docker, cross-platform (including docker-toolbox)
- Doesn't use Drupal docker image
- Is a "click & deploy" project
- Is easy to maintain
- Use custom Helm/Kubernetes chart, so it's highly customizable
- Provide both production and development environments
- Use [webflo/drupal-core-require-dev](https://packagist.org/packages/webflo/drupal-core-require-dev)

## Online installation

- Fork this project
- On the sidebar, click `Operations > Kubernetes`
- Add a Kubernetes cluster (GKE recommended)
- Once the cluster is ready, at least install
  - Helm
  - Ingress
  - GitLab-runner
- Once Ingress install is done, you'll be able to retrieve Ingress Endpoint (on the same page)
  - You'll have to fill the base domain (top page) with:
  - `YOUR_INGRESS_IP.nip.io` or
  - Use `YOUR_INGRESS_IP` to add an "A" record DNS with your domain (`*.your_domain.com`) then add `your_domain.com` as base domain.
- On the sidebar, click `Settings > CI / CD`
- Expand "Variables"
- At least, add the next variables:
  - | Name                     | Description                                                    | Default value                               |
    |--------------------------|----------------------------------------------------------------|---------------------------------------------|
    | DB_INITIALIZE            | In one line, the command to be used to initialize the website. | bash /var/www/html/scripts/db-initialize.sh |
    | DB_MIGRATE               | In one line, the command to be used on each website's upgrade. | bash /var/www/html/scripts/db-migrate.sh    |
    | K8S_SECRET_POSTGRES_SALT | The drupal's database salt.                                    | whateverYouWantAsDBSalt                     |
    | TEST_DISABLED            | GitLab Auto DevOps test jobs aren't used by default.           | true                                        |
  - Save variables
- Expand "Auto DevOps"
- Enable `Default to Auto DevOps pipeline`
- Choose your deployment strategy
- Save changes, enjoy =)

## Local installation

Linux: using bash  
Windows: using docker shell (or docker-toolbox)

### Install dependencies

```bash
$ docker run --rm --interactive --tty --volume ${PWD}:/app composer install --ignore-platform-reqs
```

### Build

```bash
$ docker-compose -f ${PWD}/docker-compose.yml build
```

### Start

```bash
$ docker-compose -f ${PWD}/docker-compose.yml up -d
$ docker exec -it autodevopsdrupal_web_1 chmod 555 /var/www/html/web/sites/default/settings.php
```

Then go to `http://${DOCKER_IP}:5000/` to get Drupal's installation page, or...

### Initialize through CLI.

After starting the container, execute
```bash
$ docker exec -it autodevopsdrupal_web_1 bash /var/www/html/scripts/db-initialize.sh
```
This is a simple shell-script which use drush to instantiate the database.

### Access website

Go to `http://${DOCKER_IP}:5000/user/login`, default authentication is `admin:admin`.

Enjoy =)

## More information

Looking for help?
- Please start with the official GitLab documentation: https://docs.gitlab.com/ce/topics/autodevops/
- If you didn't find what you were looking for, try https://forum.gitlab.com/
- If you think this project is missing something or if you discover a bug, please submit an issue [here](https://gitlab.com/le.storm1er/autodevopsdrupal/issues/new).

### Online environment architecture

The chart used is coming from the default one used by Auto DevOps, so we have 2 main pods:
- One for the database (postgres)
- One for the app (php:7.2-apache-stretch)

### Known issues

As the chart uses basic google persistent volumes, the only access mode available is `ReadWriteOnce`, therefore we can't have multiple app replicas.  
This means the scalability of this project is not perfect for now. You can still upgrade to a better cluster/node.
